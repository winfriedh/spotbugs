FROM golang:1.11 AS build
# Force the go compiler to use modules.
ENV GO111MODULE=on CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
RUN go build -o analyzer

FROM openjdk:11-jdk

RUN apt-get update && \
    apt-get install -y \
      zip

USER 0

ARG ANT_VERSION
ARG FINDSECBUGS_VERSION
ARG GRAILS_VERSION
ARG GRADLE_VERSION
ARG MAVEN_VERSION
ARG SBT_VERSION
ARG SCALA_VERSION
ARG SPOTBUGS_VERSION

ENV ANT_VERSION ${ANT_VERSION:-1.10.1}
ENV FINDSECBUGS_VERSION ${FINDSECBUGS_VERSION:-1.8.0}
ENV GRAILS_VERSION ${GRAILS_VERSION:-3.3.9}
ENV GRADLE_VERSION ${GRADLE_VERSION:-5.1}
ENV MAVEN_VERSION ${MAVEN_VERSION:-3.6.0}
ENV SBT_VERSION ${SBT_VERSION:-1.2.8}
ENV SCALA_VERSION ${SCALA_VERSION:-2.12.8}
ENV SPOTBUGS_VERSION ${SPOTBUGS_VERSION:-3.1.12}

ENV SDKMAN_SHA=8f24d694c068713d765e34667283053e2aee2051

# Install SDK man for SDK installations
RUN curl -s https://get.sdkman.io -o sdkman.sh && echo "${SDKMAN_SHA}  sdkman.sh" | sha1sum -c && /bin/bash sdkman.sh && rm sdkman.sh

# Install SDKs
RUN /bin/bash -c "\
  source $HOME/.sdkman/bin/sdkman-init.sh && \
  sdk install ant $ANT_VERSION && \
  sdk install gradle $GRADLE_VERSION && \
  sdk install grails $GRAILS_VERSION && \
  sdk install maven $MAVEN_VERSION && \
  sdk install scala $SCALA_VERSION && \
  sdk install sbt $SBT_VERSION"

# Install SpotBugs CLI
COPY spotbugs /spotbugs
RUN cd /spotbugs && \
  mkdir -p dist && \
  wget https://repo.maven.apache.org/maven2/com/github/spotbugs/spotbugs/${SPOTBUGS_VERSION}/spotbugs-${SPOTBUGS_VERSION}.tgz && \
  tar xzf spotbugs-${SPOTBUGS_VERSION}.tgz -C dist --strip-components 1 && \
  rm -f spotbugs-${SPOTBUGS_VERSION}.tgz

# Install FindSecBugs for use as a SpotBugs plugin
RUN mkdir -p /fsb && \
  cd /fsb && \
  wget https://github.com/find-sec-bugs/find-sec-bugs/releases/download/version-${FINDSECBUGS_VERSION}/findsecbugs-cli-${FINDSECBUGS_VERSION}.zip && \
  unzip -n findsecbugs-cli-${FINDSECBUGS_VERSION}.zip && \
  rm -f findsecbugs-cli-${FINDSECBUGS_VERSION}.zip && \
  mv lib/findsecbugs-plugin-${FINDSECBUGS_VERSION}.jar lib/findsecbugs-plugin.jar

# Install analyzer
COPY --from=build --chown=root:root /go/src/app/analyzer /

ENV SDK_CAND="/root/.sdkman/candidates"
ENV PATH="${SDK_CAND}/ant/current/bin:${SDK_CAND}/gradle/current/bin:${SDK_CAND}/grails/current/bin:${SDK_CAND}/maven/current/bin:${SDK_CAND}/scala/current/bin:${SDK_CAND}/sbt/current/bin:${PATH}"

ENTRYPOINT []
CMD ["/analyzer", "run"]
